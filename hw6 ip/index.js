const button = document.querySelector('.ip-button');
const ip = {
    method: 'GET',
    url: 'https://api.ipify.org/?format=json',
};
button.addEventListener('click', evt => {

    axios.request(ip).then(function (response) {
        console.log(response.data);
    const  myIP = 'http://ip-api.com/json/' + response.data.ip;
        axios.request(myIP).then(function (response) {
            button.insertAdjacentHTML("afterend", `
            <div>Timezone: ${response.data.timezone}</div>
            <div>Country: ${response.data.country}</div> 
            <div>Region code: ${response.data.region}</div>
            <div>Region: ${response.data.regionName}</div>
            <div>City: ${response.data.city}</div>
            <div>Country code: ${response.data.countryCode}</div>`)
        }).catch(function (error) {
            console.error(error);
        });
        console.log(myIP);
    }).catch(function (error) {
        console.error(error);
    });


})