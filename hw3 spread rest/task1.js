// нашел функцию в инете
Array.prototype.unique = function() {
    let a = this.concat();
    for(let i=0; i<a.length; ++i) {
        for(let j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
let clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
clients2 = [...clients2, ...clients1];
clients2 = clients2.unique();
console.log(clients2);