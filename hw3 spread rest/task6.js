const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
const extendedEmployee = {...employee, age:50, salary:1000};
console.log(extendedEmployee);