class Employee{
    constructor(name,age, salary) {
     this._name = name;
     this._age = age;
     this._salary = salary;
    }
    set name(name){
        this._name = name
    }
    get name() {
        return this._name;
    }
    set salary(salary){
        this._age = salary
    }
    get salary() {
        return this._salary;
    }
    set age(age){
        this._age = age;
    }
    get age() {
        return this._age;
    }

}
const person = new Employee("Anton",17,2900);
console.log(person);
class Programmer extends Employee{
    constructor(name,age, salary,lang) {
        super(name,age, salary);
        this._lang  = lang;
    }
    get salary(){
        return (this._salary)*3;
    }
}
const coder = new Programmer("Mark", 18, 1000, "js");
const decoder = new Programmer("Maks", 19, 2000, "c#");

console.log(coder);
console.log(decoder);