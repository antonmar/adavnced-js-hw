/*
Ответ на теор вопрос:
Конструкцию try-catch стоит использовать когда возникновение ошибки не зависит от программиста
(например если какая-то информация поступает с базы данных и мы не знаем придет ли она вообще)
 */
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },

    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const list = document.createElement("ul");
const container  = document.querySelector("#root");
container.append(list);
books.forEach(elem => {
    try {
        if (elem.author !== undefined && elem.name !== undefined && elem.price !== undefined){
            list.insertAdjacentHTML('beforeend', `
    <li>
         <div>${elem.author} </div>        
         <div>${elem.name}    </div>
         <div>${elem.price}  </div>
    </li> `)
        } else {
            throw elem;
        }
    } catch (error) {
        if(error.author === undefined){
            console.error("No author");
        }
        if(error.name === undefined){
            console.error("No name");
        }
        if(error.price === undefined){
            console.error("No price");
        }
    }
});